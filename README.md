Heroku Buildpack for Node.js and gulp.js
========================================

Usage
-----
- Set your Heroku app's buildpack URL to `https://bitbucket.org/gravityjack/sds-realty-build-pack`.
- Run `heroku config:set NODE_ENV=production` to set your environment to `production` (or any other name)
- Add a Gulp task called `heroku:production` that builds your app

Updating
--------
This project was cloned from `https://github.com/appstack/heroku-buildpack-nodejs-gulp.git` and moved to this BitBucket repo.
The need to update should be very very rare, if ever.
In the event they are necessary, updates should be pulled from that remote **before modifying** the buildpack ourselves.

You can check the current buildpack with `heroku config`.  Look for the `BUILDPACK_URL` environment variable value.

*Note, you can test another build pack on heroku by simply pointing heroku to the other buildpack's url:*  
`heroku config:set BUILDPACK_URL=<other-buildpack-url>`

**Assuming we must update this buildpack:**  
1. Clone this repo  
2. Create a remote to the original project `git remote add upstream https://github.com/appstack/heroku-buildpack-nodejs-gulp.git`  
3. Create a separate branch for comparing the update to our master:  
```
git pull origin master
git branch '<update-branch>'
git checkout <update-branch>
git pull upstream master:<update-branch>
```
4. Open a PR for the update branch into master.
5. Review, peer review, and merge.



Credits
-------

Inspired by [Deploying a Yeoman/Angular app to Heroku](http://www.sitepoint.com/deploying-yeomanangular-app-heroku/).

Forked from [heroku-buildpack-nodejs-gulp](https://github.com/timdp/heroku-buildpack-nodejs-gulp).

Which was forked from [heroku-buildpack-nodejs](https://github.com/heroku/heroku-buildpack-nodejs).

Heavily based on [heroku-buildpack-nodejs-grunt](https://github.com/mbuchetics/heroku-buildpack-nodejs-grunt).
